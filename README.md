# DEPLOY HEROKU

- pastikan sudah install heroku cli dengan menggunakan command:

```
heroku --version
```

- buat aplikasi melalui dashboard heroku
- pastikan kodingan yang akan di deploy memiliki .gitignore, yang akan mengabaikan node_modules
- tambahkan script pada package.json:

```
"start":"node index.js"
```

- pastikan port nya diganti dengan "process.env.PORT"

- login ke heroku cli
- git init (kalau belum ada git)
- heroku git:remote -a syafiq-first-deploy
