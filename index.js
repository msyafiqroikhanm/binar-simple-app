const express = require("express");
const app = express();
const port = process.env.PORT || 3000;

app.get("/", (req, res) => {
  res.send(
    "Hello BE JS 2, Im M. Syafiq RM. CI/CD with Gitlab Runner Shell Executor. Docker not running"
  );
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
